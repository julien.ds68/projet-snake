package main.snake;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.Random;

public class SpecialApple extends Apple {

    private Rectangle apple;
    private float radius=20;
    private Image img = new Image("main/resources/image/super_pomme.png");
    private int rdw;
    private int rdh;


    public SpecialApple(Group group, Plateau plateau){
        super(group,plateau);
        this.apple = new Rectangle();
    }
    public Rectangle getApple(){
        return this.apple;
    }


    public void spawn(){
        do {
            rdw = (int) (plateau.getX() + (Math.random() * (plateau.getX()+plateau.getWidth() - plateau.getX()-10)));
            rdh = (int) (plateau.getY() + (Math.random() * (plateau.getY()+plateau.getHeight() - plateau.getY()-10)));
            rdw/=10;
            rdh/=10;
            rdw*=10;
            rdh*=10;
        }while (!verifSpawn(rdw,rdh));


        apple.setX(rdw);
        apple.setY(rdh);
        apple.setWidth(radius);
        apple.setHeight(radius);
        apple.setFill(new ImagePattern(img));

        group.getChildren().add(apple);


    }
    private boolean verifSpawn(int rdw, int rdh){
        if ((double)rdw/10%2!=0 || (double)rdh/10%2!=0){
            return false;
        }
        for (Component obstacle:plateau.getListObstacle()) {
            if ((rdw < obstacle.getRectangle().getX() + 40) && (rdw > obstacle.getRectangle().getX() - 30) && (rdh < obstacle.getRectangle().getY() + 40) && (rdh > obstacle.getRectangle().getY() - 30)) {
                return false;

            }
        }
        return true;


    }

    public int getSize() {
        return 2;
    }

    public int getPoint(){
        return 5;
    }




}
