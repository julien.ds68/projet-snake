package main.snake;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class PlayClassic implements EventHandler<ActionEvent> {


    private Main m;
    private Group grpPlay;
    private Snake snake;
    private Apple apple;
    private Timer timer;
    private DropShadow ombre;
    private Rectangle complet;
    private Label gameOver;
    private Label gamePause;
    private Button reprendre;
    private Button restart;
    private Button menu;
    private Scene scenePlay;
    private Monde monde;
    private Image background;
    private Plateau plateau;
    private int vlevel, vxp, vxpToNextLevel;
    private Label labPoint, labTemps, labTitre, labBestScore;
    private boolean verifKey;
    private boolean grow;


    public PlayClassic(Main main, Monde monde) {
        this.m = main;
        this.monde = monde;
        m.profil.loadStats();
    }

    @Override
    public void handle(ActionEvent event) {
        grpPlay = new Group();
        plateau = new Plateau(grpPlay,1200,600);
        plateau.setX(40);
        plateau.setY(100);
        scenePlay = new Scene(grpPlay,Color.BLACK);
        labTitre =new Label();
        labTitre.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 80));
        labTitre.setTextFill(Color.WHITE);
        labTitre.setLayoutX(450);
        labTitre.setLayoutY(10);
        grpPlay.getChildren().add(labTitre);

        vlevel = m.profil.getLevel();
        vxp = m.profil.getExperience();
        vxpToNextLevel = m.profil.getExpToNextLevel();

        m.lvl.setLayoutY(10);
        m.xpBar.setLayoutY(50);
        m.xp.setLayoutY(80);

        grpPlay.getChildren().add(m.lvl);
        grpPlay.getChildren().add(m.xp);
        grpPlay.getChildren().add(m.xpBar);

        labTemps = new Label();


        grpPlay.getChildren().add(labTemps);

        switch (monde) {
            case PAMPA:
                plateau.creerPlateauPampa();
                labTitre.setText("La Pampa");
                break;
            case ABYSSES:
                plateau.creerPlateauAbysses();
                labTitre.setText("Les Abysses");
                break;
            case THRONE:
                plateau.creerPlateauThrone();
                labTitre.setText("Le Throne");
                break;
        }

        plateau.affichePlateau();

        snake = new Snake(m,grpPlay, plateau,m.profil.getSkinsList().get(m.profil.getSkinActivated()));
        snake.createHead(plateau.getX() , plateau.getY() ,m.profil.getUsername());
//        snake.setSkin(m.profil.getSkinsList().get(m.profil.getSkinActivated()));
        snake.show();

        labPoint = new Label("Points : "+snake.getPoint());
        labPoint.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 40));
        labPoint.setTextFill(Color.WHITE);
        labPoint.setLayoutX(80);
        labPoint.setLayoutY(30);
        grpPlay.getChildren().add(labPoint);

        labBestScore = new Label("Meilleur Score : "+m.profil.getHighscore());
        labBestScore.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        labBestScore.setLayoutX(80);
        labBestScore.setLayoutY(60);
        grpPlay.getChildren().add(labBestScore);


        chooseApple();


        scenePlay.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (!verifKey) {
                    verifKey = true;
                    if (event.getCode().toString().equalsIgnoreCase(m.conf.getUPJ1())) {
                        snake.direction(Direction.UP);
                    } else if (event.getCode().toString().equalsIgnoreCase(m.conf.getDOWNJ1())) {
                        snake.direction(Direction.DOWN);
                    } else if (event.getCode().toString().equalsIgnoreCase(m.conf.getLEFTJ1())) {
                        snake.direction(Direction.LEFT);
                    } else if (event.getCode().toString().equalsIgnoreCase(m.conf.getRIGHTJ1())) {
                        snake.direction(Direction.RIGHT);
                    } else if (event.getCode().toString().equalsIgnoreCase("ESCAPE")) {
                        if (snake.isInPause()) {
                            snake.endPause();
                        } else if (!snake.isInPause()) {
                            snake.pause();
                            gamepause();
                        }
                    } else if (event.getCode().toString().equalsIgnoreCase("SHIFT")) {
                        snake.addLenght();
                        incrementerExperience(10);
                    }
                }
            }
        });

        playTime(60);

        m.primaryStage.setScene(scenePlay);


    }
    public void chooseApple() {
        Random chanceSpawn = new Random();
        if (chanceSpawn.nextInt(100) <= 30){
            apple = new SpecialApple(grpPlay,plateau);
        }else{
            apple = new Apple(grpPlay,plateau);
        }
        apple.spawn();

    }

    public void playTime(int fps){
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                Platform.runLater(() -> {

                    if (snake.isAlive() && !snake.sortiDuPlateau() && !snake.toucheSonCorps() && !snake.isInPause()) {

                        if (grow){
                            snake.addLenght(apple.getSize());
                            grow=false;
                        }else{
                            snake.update();
                        }

                        verifKey=false;
                        snake.show();
                        if ((snake.getHead().getX() == apple.getApple().getX()) && (snake.getHead().getY() == apple.getApple().getY())) {
                            grpPlay.getChildren().remove(apple.getApple());
                            snake.addPoint(apple.getPoint());
                            grow=true;
                            incrementerExperience(apple.getPoint()*10);
                            labPoint.setText("Points : "+snake.getPoint());
                            chooseApple();

                        }
                        for (Obstacle obstacle : plateau.getListObstacle()){
                            if ((snake.getHead().getX() < obstacle.getRectangle().getX()+30) && (snake.getHead().getX() > obstacle.getRectangle().getX()-20) && (snake.getHead().getY() < obstacle.getRectangle().getY()+30) && (snake.getHead().getY() > obstacle.getRectangle().getY()-20)){
                                snake.dead();
                            }
                        }
                    } else if (!snake.isAlive()) {
                        gameover();
                        timer.cancel();
                    }

                });


            }
        }, 1000, fps);
    }

    public void gamepause(){

        ombre = new DropShadow();
        ombre.setRadius(5.0);
        ombre.setOffsetX(3.0);
        ombre.setOffsetY(3.0);
        ombre.setColor(Color.color(0.9, 0.9, 0.9));
        complet = new Rectangle();
        complet.setEffect(ombre);
        complet.setX(m.primaryStage.getWidth() / 2 - 180);
        complet.setY(m.primaryStage.getHeight() / 2 - 150);
        complet.setHeight(400);
        complet.setWidth(300);
        complet.setArcHeight(50);
        complet.setArcWidth(50);
        complet.setStroke(Color.web("#BFE600"));
        grpPlay.getChildren().add(complet);

        gamePause= new Label("GAME PAUSED");
        gamePause.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        gamePause.setId("textover");
        gamePause.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        gamePause.setLayoutX(m.primaryStage.getWidth() / 2 - 168);
        gamePause.setLayoutY(m.primaryStage.getHeight() / 2 -90);
        grpPlay.getChildren().add(gamePause);

        reprendre = new Button();
        reprendre.setText("Resume");
        reprendre.setFont(Font.font ("Verdana", 20));
        reprendre.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                endGamePause();
            }
        });
        reprendre.setLayoutX(m.primaryStage.getWidth() / 2- 91);
        reprendre.setLayoutY(m.primaryStage.getHeight() / 2 + 70);
        reprendre.setTextFill(Paint.valueOf("#ffffff"));
        reprendre.setStyle("-fx-background-color: #83da00");
        grpPlay.getChildren().add(reprendre);

        menu = new Button();
        menu.setText("Quitter");
        menu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.creerMenu();
            }
        });
        menu.setTextFill(Paint.valueOf("#ffffff"));
        menu.setFont(Font.font ("Verdana", 20));
        menu.setLayoutX(m.primaryStage.getWidth() / 2 - 85);
        menu.setLayoutY(m.primaryStage.getHeight() / 2 + 130);
        menu.setStyle("-fx-background-color: #83da00");
        grpPlay.getChildren().add(menu);


    }

    private void endGamePause() {
        snake.endPause();
        grpPlay.getChildren().remove(complet);
        grpPlay.getChildren().remove(gamePause);
        grpPlay.getChildren().remove(reprendre);
        grpPlay.getChildren().remove(menu);
        playTime(60);
    }


    public void gameover() {

        scenePlay.setOnKeyPressed(null);

        saveToFile();
        ombre = new DropShadow();
        ombre.setRadius(5.0);
        ombre.setOffsetX(3.0);
        ombre.setOffsetY(3.0);
        ombre.setColor(Color.color(0.9, 0.9, 0.9));
        complet = new Rectangle();
        complet.setEffect(ombre);
        complet.setX(m.primaryStage.getWidth() / 2 - 180);
        complet.setY(m.primaryStage.getHeight() / 2 - 150);
        complet.setHeight(400);
        complet.setWidth(300);
        complet.setArcHeight(50);
        complet.setArcWidth(50);
        complet.setStroke(Color.web("#BFE600"));
        grpPlay.getChildren().add(complet);

        gameOver = new Label("GAME OVER");
        gameOver.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        gameOver.setId("textover");
        gameOver.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        gameOver.setLayoutX(m.primaryStage.getWidth() / 2 - 140);
        gameOver.setLayoutY(m.primaryStage.getHeight() / 2 -90);
        grpPlay.getChildren().add(gameOver);

        if(snake.getPoint()<m.profil.getHighscore()) {
            labPoint = new Label("Votre score : " + snake.getPoint());
            labPoint.setLayoutX(m.primaryStage.getWidth() / 2 - 90);
            labPoint.setLayoutY(m.primaryStage.getHeight() / 2);
            labPoint.setTextFill(Color.web("#E6E6E6"));
            grpPlay.getChildren().add(labPoint);
        }else if(snake.getPoint()==m.profil.getHighscore()){
            labPoint = new Label("Vous avez battu votre record : " + snake.getPoint());
            labPoint.setLayoutX(m.primaryStage.getWidth() / 2 - 130);
            labPoint.setLayoutY(m.primaryStage.getHeight() / 2);
            labPoint.setTextFill(Color.web("#E6E6E6"));
            grpPlay.getChildren().add(labPoint);
        }

        restart = new Button();
        restart.setText("Relancer");
        restart.setFont(Font.font ("Verdana", 20));
        restart.setOnAction(this::handle);
        restart.setLayoutX(m.primaryStage.getWidth() / 2- 91);
        restart.setLayoutY(m.primaryStage.getHeight() / 2 + 70);
        restart.setTextFill(Paint.valueOf("#ffffff"));
        restart.setStyle("-fx-background-color: #83da00");
        grpPlay.getChildren().add(restart);

        menu = new Button();
        menu.setText("Quitter");
        menu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.creerMenu();
            }
        });
        menu.setTextFill(Paint.valueOf("#ffffff"));
        menu.setFont(Font.font ("Verdana", 20));
        menu.setLayoutX(m.primaryStage.getWidth() / 2 - 83);
        menu.setLayoutY(m.primaryStage.getHeight() / 2 + 130);
        menu.setStyle("-fx-background-color: #83da00");
        grpPlay.getChildren().add(menu);
    }

    public void saveToFile(){
        m.profil.tryChangeHighscore(snake.getPoint());
        m.profil.setLevel(vlevel);
        m.profil.setExperience(vxp);
        m.profil.setExpToNextLevel(vxpToNextLevel);
        m.profil.saveFileProfil();
    }

    public void incrementerExperience(int nbAjouter){
        int newxp = vxp+nbAjouter;
        if (vxpToNextLevel > newxp){
            vxp = newxp;
        }else{
            if (newxp == vxpToNextLevel){
                vlevel++;
                vxp=0;
                vxpToNextLevel = (int) Math.round((vxpToNextLevel*1.25)-(vxpToNextLevel*1.25)%10);
            }else{
                int xprestant = newxp-vxpToNextLevel;
                vlevel++;
                vxpToNextLevel = ((int) Math.round((vxpToNextLevel*1.25)-(vxpToNextLevel*1.25)%10));
                while (xprestant>=vxpToNextLevel){
                    xprestant=xprestant-vxpToNextLevel;
                    vlevel++;
                    vxpToNextLevel = ((int) Math.round((vxpToNextLevel*1.25)-(vxpToNextLevel*1.25)%10));
                }
                vxp = xprestant;
            }
        }
        m.reloadAffStat(vlevel, vxp, vxpToNextLevel);
    }

}


