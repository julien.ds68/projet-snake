package main.snake;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.*;
import java.util.Timer;


public class Main extends Application {

    Button btnPlay = new Button();
    Button btn1v1 = new Button();
    Button btnSetting = new Button();
    Button btnQuit = new Button();
    Button btnCrossLeave = new Button();
    Button btnMinimize = new Button();
    Group grpMenu = new Group();
    Button previousSkin = new Button();
    Button nextSkin = new Button();
    Scene sceneMenu = new Scene(grpMenu);
    ProgressBar xpBar = new ProgressBar();
    boolean isFullScreen = false;
    private Timer timer;
    Config conf;
    ProfilFile profil;
    Label titre, lvl, xp;
    Snake abysses;

    ChooseMap chooseMap;
    ChooseMode chooseMode;
    Settings setting;
    Stage primaryStage;
    Glow glow;

    int LARGEUR  = 1280;
    int HAUTEUR = 720;
    private double xOffset;
    private double yOffset;

    public static void main(String[] args) {
        Application.launch(Main.class, args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        conf = new Config(this);
        profil = new ProfilFile(this);

        this.primaryStage=primaryStage;
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setTitle("Snake");
        primaryStage.setHeight(HAUTEUR);
        primaryStage.setWidth(LARGEUR);
        primaryStage.setMinHeight(HAUTEUR);
        primaryStage.setMinWidth(LARGEUR);

        initMenu();
        creerMenu();

        primaryStage.show();
    }

    public void initMenu(){
        sceneMenu.setFill(Paint.valueOf("#000000"));

        chooseMap = new ChooseMap(this);
        chooseMode = new ChooseMode(this);
        setting = new Settings(this);

        btnMinimize.setText("_");
        btnMinimize.setMinSize(50,50);
        btnMinimize.setTextAlignment(TextAlignment.RIGHT);
        btnMinimize.setTextFill(Paint.valueOf("#ffffff"));
        btnMinimize.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnMinimize.setId("btnMinimize");
        btnMinimize.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnMinimize.setBackground(null);
        btnMinimize.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                primaryStage.setIconified(true);
            }
        });

        btnCrossLeave.setText("X");
        btnCrossLeave.setMinSize(50,50);
        btnCrossLeave.setTextFill(Paint.valueOf("#ffffff"));
        btnCrossLeave.setTextAlignment(TextAlignment.LEFT);
        btnCrossLeave.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnCrossLeave.setId("btnCrossLeave");
        btnCrossLeave.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnCrossLeave.setBackground(null);
        btnCrossLeave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });

        glow = new Glow();
        glow.setLevel(20);

        titre = new Label("SNAKE");
        titre.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 100));
        titre.setTextFill(Paint.valueOf("#ffffff"));
        titre.setEffect(glow);

        ImageView imgPrevious = new ImageView(new Image(getClass().getResource("../resources/image/UpArrow.png").toExternalForm()));
        imgPrevious.setFitWidth(50);
        imgPrevious.setFitHeight(75);

        previousSkin.setText("");
        previousSkin.setBackground(null);
        previousSkin.setGraphic(imgPrevious);
        previousSkin.setMaxSize(50,75);
        previousSkin.setId("previousSkin");
        previousSkin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                previousSkin();
            }
        });
        previousSkin.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());

        ImageView img = new ImageView(new Image(getClass().getResource("../resources/image/DownArrow.png").toExternalForm()));
        img.setFitWidth(50);
        img.setFitHeight(75);

        nextSkin.setText("");
        nextSkin.setBackground(null);
        nextSkin.setMaxSize(50,75);
        nextSkin.setGraphic(img);
        nextSkin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nextSkin();
            }
        });
        nextSkin.setId("nextSkin");
        nextSkin.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());

        btnPlay.setText("Classique");
        btnPlay.setMinSize(150,50);
        btnPlay.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnPlay.setTextFill(Paint.valueOf("#ffffff"));
        btnPlay.setOnAction(chooseMap);
        btnPlay.setId("btnPlay");
        btnPlay.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnPlay.setBackground(null);

        btn1v1.setText("1V1");
        btn1v1.setMinSize(150,50);
        btn1v1.setTextFill(Paint.valueOf("#ffffff"));
        btn1v1.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btn1v1.setOnAction(chooseMode);
        btn1v1.setId("btn1v1");
        btn1v1.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btn1v1.setBackground(null);

        btnSetting.setText("Options");
        btnSetting.setMinSize(150,50);
        btnSetting.setTextFill(Paint.valueOf("#ffffff"));
        btnSetting.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnSetting.setOnAction(setting);
        btnSetting.setId("btnSetting");
        btnSetting.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnSetting.setBackground(null);

        btnQuit.setText("Quitter");
        btnQuit.setMinSize(150,50);
        btnQuit.setTextFill(Paint.valueOf("#ffffff"));
        btnQuit.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnQuit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
        btnQuit.setId("btnQuit");
        btnQuit.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnQuit.setBackground(null);

        lvl = new Label();
        lvl.setId("lvl");
        lvl.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        lvl.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 40));
        lvl.setTextFill(Paint.valueOf("#ffffff"));

        xpBar.setId("xpBar");
        xpBar.setMinWidth(225);
        xpBar.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());

        xp = new Label();
        xp.setId("xp");
        xp.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        xp.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        xp.setTextFill(Paint.valueOf("#ffffff"));
    }

    public void creerMenu(){

        profil.loadStats();

        grpMenu.getChildren().clear();

        if (isFullScreen){
            primaryStage.setMaximized(true);
            sceneMenu.setOnMousePressed(null);
            sceneMenu.setOnMouseDragged(null);
        }else {
            primaryStage.setMaximized(false);
            sceneMenu.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = primaryStage.getX() - event.getScreenX();
                    yOffset = primaryStage.getY() - event.getScreenY();
                }
            });

            sceneMenu.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    primaryStage.setX(event.getScreenX() + xOffset);
                    primaryStage.setY(event.getScreenY() + yOffset);
                }
            });
        }

        titre.setLayoutX(primaryStage.getWidth()/2-150);
        titre.setLayoutY(primaryStage.getHeight()/8);
        grpMenu.getChildren().add(titre);

        previousSkin.setLayoutX(primaryStage.getWidth()/8);
        previousSkin.setLayoutY(primaryStage.getHeight()/2-100);
        grpMenu.getChildren().add(previousSkin);
        abysses = new Snake(this,grpMenu,null, profil.getSkinsList().get(profil.getSkinActivated()));
		affSkin();
		reloadSkin();

        nextSkin.setLayoutX(primaryStage.getWidth()/8);
        nextSkin.setLayoutY(primaryStage.getHeight()/2+100);
        grpMenu.getChildren().add(nextSkin);

        btnMinimize.setLayoutX(primaryStage.getWidth()-150);
        btnMinimize.setLayoutY(-5);
        grpMenu.getChildren().add(btnMinimize);

        btnCrossLeave.setLayoutX(primaryStage.getWidth()-90);
        btnCrossLeave.setLayoutY(0);
        grpMenu.getChildren().add(btnCrossLeave);

        btnPlay.setLayoutX(primaryStage.getWidth()/2-150);
        btnPlay.setLayoutY(primaryStage.getHeight()/4+50);
        grpMenu.getChildren().add(btnPlay);

        btn1v1.setLayoutX(primaryStage.getWidth()/2-150);
        btn1v1.setLayoutY(primaryStage.getHeight()/4+150);
        grpMenu.getChildren().add(btn1v1);

        btnSetting.setLayoutX(primaryStage.getWidth()/2-150);
        btnSetting.setLayoutY(primaryStage.getHeight()/4+250);
        grpMenu.getChildren().add(btnSetting);

        btnQuit.setLayoutX(primaryStage.getWidth()/2-150);
        btnQuit.setLayoutY(primaryStage.getHeight()/4+350);
        grpMenu.getChildren().add(btnQuit);

        lvl.setText("Level : " + profil.getLevel());
        lvl.setEffect(glow);
        lvl.setLayoutX(1000);
        lvl.setLayoutY(primaryStage.getHeight()/8+10);
        grpMenu.getChildren().add(lvl);

        xpBar.setLayoutX(950);
        xpBar.setLayoutY(primaryStage.getHeight()/8+50);
        xpBar.setProgress(profil.getExperience()/Double.valueOf(profil.getExpToNextLevel()));
        grpMenu.getChildren().add(xpBar);

        xp.setText(profil.getExperience() + " / " + profil.getExpToNextLevel());
        xp.setLayoutX(1025);
        xp.setLayoutY(primaryStage.getHeight()/8+80);
        grpMenu.getChildren().add(xp);

        primaryStage.setScene(sceneMenu);
    }
    public void reloadAffStat(int level, int exp, int xpnextlvl){
        xpBar.setProgress(exp/Double.valueOf(xpnextlvl));
        xp.setText(exp + " / " + xpnextlvl);
        lvl.setText("Level : " + level);
    }

    public void affSkin(){
    	abysses.createHead(primaryStage.getWidth()/8, primaryStage.getHeight()/2+25, null);
    	abysses.setSkin(profil.getSkinsList().get(profil.getSkinActivated()));
        abysses.addLenght(4);
        abysses.show();
    }

    public void reloadSkin(){
        abysses.reloadSkin();
        profil.saveFileProfil();
    }

    public void nextSkin(){
        if (profil.getSkinActivated() != profil.getSkinsList().size()-1){
            profil.setSkinActivate(profil.getSkinActivated()+1);
            abysses.setSkin(profil.getSkinsList().get(profil.getSkinActivated()));
            System.out.println("Skin activer : " + profil.getSkinsList().get(profil.getSkinActivated()));
        }else{
            profil.setSkinActivate(0);
            abysses.setSkin("default");
        }
        reloadSkin();
    }

    public void previousSkin(){
        if (profil.getSkinActivated() != 0){
            profil.setSkinActivate(profil.getSkinActivated()-1);
            abysses.setSkin(profil.getSkinsList().get(profil.getSkinActivated()));
            System.out.println("Skin activer : " + profil.getSkinsList().get(profil.getSkinActivated()));
        }else{
            profil.setSkinActivate(profil.getSkinsList().size()-1);
            abysses.setSkin(profil.getSkinsList().get(profil.getSkinsList().size()-1));
        }
        reloadSkin();
    }
}
