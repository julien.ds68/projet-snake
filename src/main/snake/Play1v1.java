package main.snake;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Play1v1 implements EventHandler<ActionEvent> {

    private Main m;
    private Mode mode;
    private Group grp1v1;
    private Snake snake1;
    private Snake snake2;
    private Timer timer;
    private int i=0;
    private Label win;
    private DropShadow ombre;
    private Rectangle complet;
    private Label gameOver;
    private Button restart;
    private Button menu;
    private Scene scene1v1;
    private Plateau plateau;
    private IA ia;
    private boolean grow;
    private boolean isDeadSnake1, isDeadSnake2;


    public Play1v1(Main main, Mode mode) {
        this.m = main;
        this.mode = mode;
    }

    @Override
    public void handle(ActionEvent event) {

        grp1v1 = new Group();
        scene1v1 = new Scene(grp1v1, Color.BLACK);
        plateau = new Plateau(grp1v1,(int)m.primaryStage.getWidth()-40,(int)m.primaryStage.getHeight()-40);
        plateau.setX(20);
        plateau.setY(20);
        plateau.creerPlateauJoueurContreJoueur();
        plateau.affichePlateau();

        Random rd = new Random();
        String skinOther = m.profil.getAllSkin().get(rd.nextInt(m.profil.getAllSkin().size()));
        snake1 = new Snake(m,grp1v1,plateau,m.profil.getSkinsList().get(m.profil.getSkinActivated()));
        snake2 = new Snake(m,grp1v1,plateau,m.profil.getSkinsList().get(m.profil.getSkinActivated()));
        snake2.forceSkin(skinOther);
        snake1.createHead(20,20,m.profil.getUsername());
        snake2.createHead((int)m.primaryStage.getWidth()-40,(int)m.primaryStage.getHeight()-40,"Guest");
        snake2.direction(Direction.LEFT);

        if (mode == Mode.PLAYER){
            scene1v1.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if (event.getCode().toString().equalsIgnoreCase(m.conf.getUPJ1())){
                        snake1.direction(Direction.UP);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getDOWNJ1())){
                        snake1.direction(Direction.DOWN);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getLEFTJ1())){
                        snake1.direction(Direction.LEFT);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getRIGHTJ1())){
                        snake1.direction(Direction.RIGHT);
                    }else if (event.getCode().toString().equalsIgnoreCase(m.conf.getUPJ2())){
                        snake2.direction(Direction.UP);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getDOWNJ2())){
                        snake2.direction(Direction.DOWN);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getLEFTJ2())){
                        snake2.direction(Direction.LEFT);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getRIGHTJ2())){
                        snake2.direction(Direction.RIGHT);
                    }else if(event.getCode().toString().equalsIgnoreCase("SHIFT")){
                        snake1.addLenght();
                    }

                }

            });
        }else if (mode == Mode.IA){
            scene1v1.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if (event.getCode().toString().equalsIgnoreCase(m.conf.getUPJ1())){
                        snake1.direction(Direction.UP);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getDOWNJ1())){
                        snake1.direction(Direction.DOWN);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getLEFTJ1())){
                        snake1.direction(Direction.LEFT);
                    }else if(event.getCode().toString().equalsIgnoreCase(m.conf.getRIGHTJ1())){
                        snake1.direction(Direction.RIGHT);
                    }

                }
            });
            ia = new IA(snake2, snake1, plateau, null, Direction.LEFT);
        }

        snake1.show();
        snake2.show();
        isDeadSnake1 = false;
        isDeadSnake2 = false;
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                Platform.runLater(() -> {

                    if (!snake1.sortiDuPlateau() && !snake2.sortiDuPlateau()) {
                        if (grow){
                            snake1.addLenght();
                            if (mode == Mode.IA){
                                ia.play(grow);
                            }else{
                                snake2.addLenght();
                                snake2.show();
                            }
                            grow=false;
                        }else {
                            snake1.update();
                            if (mode == Mode.IA) {
                                ia.play(grow);
                            } else {
                                snake2.update();
                                snake2.show();
                            }
                        }
                        snake1.show();



                        if (snake1.hit(snake2)){
                            isDeadSnake1 = true;
                        }

                        if (snake2.hit(snake1)){
                            isDeadSnake2 = true;
                        }

                        if (isDeadSnake2 || isDeadSnake1){
                            if (isDeadSnake1 && !isDeadSnake2){
                                gameOver(snake2.getName());
                                snake1.dead();
                                timer.cancel();
                            }else if(isDeadSnake2 && !isDeadSnake1){
                                gameOver(snake1.getName());
                                snake2.dead();
                                timer.cancel();
                            }else if (isDeadSnake1 && isDeadSnake2){
                                gameOver("Egaliter");
                                snake1.dead();
                                snake2.dead();
                                timer.cancel();
                            }
                        }

                        i++;
                        if (i == 40) {
                            i = 0;
                            grow=true;
                        }
                    }
                    if (snake1.sortiDuPlateau() || snake2.sortiDuPlateau()){
                        if (snake1.sortiDuPlateau() && !snake2.sortiDuPlateau()){
                            gameOver(snake2.getName());
                            timer.cancel();
                        }else if(snake2.sortiDuPlateau() && !snake1.sortiDuPlateau()){
                            gameOver(snake1.getName());
                            timer.cancel();
                        }else if (snake1.sortiDuPlateau() && snake2.sortiDuPlateau()){
                            gameOver("Egaliter");
                            snake1.dead();
                            snake2.dead();
                            timer.cancel();
                        }
                    }

                });


            }
        }, 1000, 60);





        m.primaryStage.setScene(scene1v1);



    }


    public void gameOver(String gagnant){
        String varAff = "Bravo";
        String textAff = gagnant + " gagne la partie";
        double textAffX = m.primaryStage.getWidth()/2-115;
        scene1v1.setOnKeyPressed(null);

        ombre = new DropShadow();
        ombre.setRadius(5.0);
        ombre.setOffsetX(3.0);
        ombre.setOffsetY(3.0);
        ombre.setColor(Color.color(0.9, 0.9, 0.9));
        complet = new Rectangle();
        complet.setEffect(ombre);
        complet.setX(m.primaryStage.getWidth() / 2 - 180);
        complet.setY(m.primaryStage.getHeight() / 2 - 150);
        complet.setHeight(400);
        complet.setWidth(300);
        complet.setArcHeight(50);
        complet.setArcWidth(50);
        complet.setStroke(Color.web("#BFE600"));
        grp1v1.getChildren().add(complet);

        if (gagnant.equalsIgnoreCase("Egaliter")){
            varAff = "Egaliter";
            textAff = "Réessayer la prochaine fois";
            textAffX = m.primaryStage.getWidth()/2-150;
        }

        gameOver = new Label(varAff);
        gameOver.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        gameOver.setId("1v1over");
        gameOver.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        gameOver.setLayoutX(m.primaryStage.getWidth() / 2 - 80);
        gameOver.setLayoutY(m.primaryStage.getHeight() / 2 -90);
        grp1v1.getChildren().add(gameOver);

        win= new Label();
        win.setText(textAff);
        win.setFont(Font.font ("Verdana", 18));
        win.setTextFill(Color.web("#E6E6E6"));
        win.setLayoutX(textAffX);
        win.setLayoutY(m.primaryStage.getHeight()/2);
        grp1v1.getChildren().add(win);

        restart = new Button();
        restart.setText("Relancer");
        restart.setFont(Font.font ("Verdana", 20));
        restart.setOnAction(this::handle);
        restart.setLayoutX(m.primaryStage.getWidth() / 2- 91);
        restart.setLayoutY(m.primaryStage.getHeight() / 2 + 70);
        restart.setTextFill(Paint.valueOf("#ffffff"));
        restart.setStyle("-fx-background-color: #83da00");
        grp1v1.getChildren().add(restart);

        menu = new Button();
        menu.setText("Quitter");
        menu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.creerMenu();
            }
        });
        menu.setTextFill(Paint.valueOf("#ffffff"));
        menu.setFont(Font.font ("Verdana", 20));
        menu.setLayoutX(m.primaryStage.getWidth() / 2 - 83);
        menu.setLayoutY(m.primaryStage.getHeight() / 2 + 130);
        menu.setStyle("-fx-background-color: #83da00");
        grp1v1.getChildren().add(menu);

    }
}

