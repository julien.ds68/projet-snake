package main.snake;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

public class ChooseMap implements EventHandler<ActionEvent> {
    Main m;

    Label labGeneral, labMap1, labMap2,labMap3, labDebloqueMap2, labDebloqueMap3;

    Button btnBack,btnMinimize,btnCrossLeave,btnMap1,btnMap2, btnMap3;

    Group grpChooseMap;
    PlayClassic playClassic;

    Scene sceneChooseMap;

    private double xOffset,yOffset;


    public ChooseMap(Main main) {
        this.m = main;

    }


    @Override
    public void handle(ActionEvent event) {
        m.profil.loadStats();
        m.reloadAffStat(m.profil.getLevel(), m.profil.getExperience(), m.profil.getExpToNextLevel());
        grpChooseMap = new Group();
        sceneChooseMap = new Scene(grpChooseMap, Color.BLACK);

        btnBack = new Button("Retour");
        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.creerMenu();
            }
        });
        btnBack.setId("btnBack");
        btnBack.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnBack.setBackground(null);
        btnBack.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnBack.setMinSize(150,50);
        btnBack.setLayoutX(100);
        btnBack.setLayoutY(50);
        grpChooseMap.getChildren().add(btnBack);



        if (m.isFullScreen){
            m.primaryStage.setMaximized(true);
            sceneChooseMap.setOnMousePressed(null);
            sceneChooseMap.setOnMouseDragged(null);
        }else {
            m.primaryStage.setMaximized(false);
            sceneChooseMap.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = m.primaryStage.getX() - event.getScreenX();
                    yOffset = m.primaryStage.getY() - event.getScreenY();
                }
            });

            sceneChooseMap.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    m.primaryStage.setX(event.getScreenX() + xOffset);
                    m.primaryStage.setY(event.getScreenY() + yOffset);
                }
            });
        }


        grpChooseMap.getChildren().add(m.btnMinimize);
        grpChooseMap.getChildren().add(m.btnCrossLeave);




        grpChooseMap.getChildren().add(m.lvl);
        grpChooseMap.getChildren().add(m.xp);
        grpChooseMap.getChildren().add(m.xpBar);

        labGeneral = new Label("Choisir un monde");

        labMap1 = new Label("La Pampa");
        labMap2 = new Label("Les Abysses");
        labMap3 = new Label("Le Throne");



        labGeneral.setTextFill(Paint.valueOf("#ffffff"));
        labGeneral.setLayoutX(400);
        labGeneral.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 60));
        labGeneral.setLayoutY(150);

        labMap1.setTextFill(Paint.valueOf("#ffffff"));
        labMap1.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 40));
        labMap1.setLayoutX(100);
        labMap1.setLayoutY(300);



        labMap2.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 40));
        labMap2.setLayoutX(515);
        labMap2.setLayoutY(300);


        labMap3.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 40));
        labMap3.setLayoutX(930);
        labMap3.setLayoutY(300);

        grpChooseMap.getChildren().add(labGeneral);
        grpChooseMap.getChildren().add(labMap1);
        grpChooseMap.getChildren().add(labMap2);
        grpChooseMap.getChildren().add(labMap3);

        btnMap1 = new Button("Jouer");
        btnMap2 = new Button();
        btnMap3 = new Button();

        BackgroundImage backgroundImage = new BackgroundImage( new Image( getClass().getResource("../resources/image/monde1.png").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);
        btnMap1.setMinWidth(250);
        btnMap1.setMinHeight(250);
        btnMap1.setLayoutX(100);
        btnMap1.setLayoutY(350);
        btnMap1.getStyleClass().add("btnChooseMap");
        btnMap1.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnMap1.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnMap1.setTextFill(Paint.valueOf("#ffffff"));
        btnMap1.setOnAction(new PlayClassic(m,Monde.PAMPA));
        btnMap1.setTextFill(Color.WHITE);
        btnMap1.setBackground(background);




        BackgroundImage backgroundImage2 = new BackgroundImage( new Image( getClass().getResource("../resources/image/monde2.png").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background2 = new Background(backgroundImage2);
        btnMap2.setMinWidth(250);
        btnMap2.setMinHeight(250);
        btnMap2.setLayoutX(515);
        btnMap2.setLayoutY(350);
        btnMap2.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnMap2.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnMap2.setTextFill(Color.WHITE);
        btnMap2.setOnAction(new PlayClassic(m,Monde.ABYSSES));
        btnMap2.setBackground(background2);


        BackgroundImage backgroundImage3 = new BackgroundImage( new Image( getClass().getResource("../resources/image/monde3.png").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background3 = new Background(backgroundImage3);
        btnMap3.setMinWidth(250);
        btnMap3.setMinHeight(250);
        btnMap3.setLayoutX(930);
        btnMap3.setLayoutY(350);
        btnMap3.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnMap3.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnMap3.setTextFill(Color.WHITE);
        btnMap3.setOnAction(new PlayClassic(m,Monde.THRONE));
        btnMap3.setBackground(background3);


        grpChooseMap.getChildren().add(btnMap1);
        grpChooseMap.getChildren().add(btnMap2);
        grpChooseMap.getChildren().add(btnMap3);


        labDebloqueMap2 = new Label("Niveau 10 pour débloquer ce monde");
        labDebloqueMap3 = new Label("Niveau 20 pour débloquer ce monde");

        labDebloqueMap2.setLayoutX(515);
        labDebloqueMap2.setLayoutY(600);

        labDebloqueMap3.setLayoutX(930);
        labDebloqueMap3.setLayoutY(600);




        if (m.profil.getLevel()>=10){
            labMap2.setTextFill(Paint.valueOf("#ffffff"));
            btnMap2.setText("Jouer");
            btnMap2.getStyleClass().add("btnChooseMap");

        }else {
            grpChooseMap.getChildren().add(labDebloqueMap2);
            btnMap2.setDisable(true);
            btnMap2.getStyleClass().add("btnChooseMapNotUnlocked");
        }
        if (m.profil.getLevel()>=20){
            labMap3.setTextFill(Paint.valueOf("#ffffff"));
            btnMap3.setText("Jouer");
            btnMap3.getStyleClass().add("btnChooseMap");

        }else {
            grpChooseMap.getChildren().add(labDebloqueMap3);
            btnMap3.setDisable(true);
            btnMap3.getStyleClass().add("btnChooseMapNotUnlocked");
        }



        m.primaryStage.setScene(sceneChooseMap);


    }
}
