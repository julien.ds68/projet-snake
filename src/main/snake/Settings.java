package main.snake;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

public class Settings implements EventHandler<ActionEvent> {

    private Main m;
    private Group grpSettings;
    private Button btnBack, btnValid, btnCrossLeave, btnMinimize;

    private Button btnJ1Haut, btnJ1Bas, btnJ1Gauche, btnJ1Droite;
    private Button btnJ2Haut, btnJ2Bas, btnJ2Gauche, btnJ2Droite;
    private TextField username;
    Scene sceneSettings;

    private Label j1Label, j2Label, usernameLabel;
    private double xOffset;
    private double yOffset;

    public Settings(Main main) {
        this.m = main;
    }

    @Override
    public void handle(ActionEvent event) {
        grpSettings = new Group();
        sceneSettings = new Scene(grpSettings, Color.BLACK);

        m.primaryStage.setScene(sceneSettings);

        btnBack = new Button("Retour");
        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.creerMenu();
            }
        });



        if (m.isFullScreen){
            m.primaryStage.setMaximized(true);
            sceneSettings.setOnMousePressed(null);
            sceneSettings.setOnMouseDragged(null);
        }else {
            m.primaryStage.setMaximized(false);
            sceneSettings.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = m.primaryStage.getX() - event.getScreenX();
                    yOffset = m.primaryStage.getY() - event.getScreenY();
                }
            });

            sceneSettings.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    m.primaryStage.setX(event.getScreenX() + xOffset);
                    m.primaryStage.setY(event.getScreenY() + yOffset);
                }
            });
        }

        btnMinimize = new Button();
        btnMinimize.setText("_");
        btnMinimize.setMinSize(50,50);
        btnMinimize.setLayoutX(m.primaryStage.getWidth()-150);
        btnMinimize.setLayoutY(-5);
        btnMinimize.setTextFill(Paint.valueOf("#ffffff"));
        btnMinimize.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnMinimize.setId("btnMinimize");
        btnMinimize.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnMinimize.setBackground(null);
        btnMinimize.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.primaryStage.setIconified(true);
            }
        });
        grpSettings.getChildren().add(btnMinimize);

        btnCrossLeave = new Button();
        btnCrossLeave.setText("X");
        btnCrossLeave.setMinSize(50,50);
        btnCrossLeave.setLayoutX(m.primaryStage.getWidth()-90);
        btnCrossLeave.setLayoutY(0);
        btnCrossLeave.setTextFill(Paint.valueOf("#ffffff"));
        btnCrossLeave.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnCrossLeave.setId("btnCrossLeave");
        btnCrossLeave.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnCrossLeave.setBackground(null);
        btnCrossLeave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
        grpSettings.getChildren().add(btnCrossLeave);


        btnBack.setId("btnBack");
        btnBack.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnBack.setBackground(null);
        btnBack.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnBack.setMinSize(150,50);
        btnBack.setLayoutX(m.primaryStage.getWidth()/8);
        btnBack.setLayoutY(150);
        grpSettings.getChildren().add(btnBack);

        usernameLabel = new Label("Username :");
        usernameLabel.setLayoutX(m.primaryStage.getWidth()/2-120);
        usernameLabel.setLayoutY(m.primaryStage.getHeight()/8);
        usernameLabel.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        usernameLabel.setTextFill(Paint.valueOf("#ffffff"));
        usernameLabel.setEffect(m.glow);
        grpSettings.getChildren().add(usernameLabel);

        username = new TextField();
        username.setText(m.profil.getUsername());
        username.setLayoutX(m.primaryStage.getWidth()/2-120);
        username.setLayoutY(m.primaryStage.getHeight()/8+50);
        username.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        username.setId("username");
        username.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        username.setBackground(null);
        grpSettings.getChildren().add(username);

        j1Label = new Label("Joueur 1 :");
        j1Label.setLayoutX(m.primaryStage.getWidth()/2-120);
        j1Label.setLayoutY(m.primaryStage.getHeight()/8+100);
        j1Label.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        j1Label.setTextFill(Paint.valueOf("#ffffff"));
        j1Label.setEffect(m.glow);
        grpSettings.getChildren().add(j1Label);

        btnJ1Haut = new Button(getSpecialChar(m.conf.getUPJ1()));
        btnJ1Haut.getStyleClass().add("btnTouche");
        btnJ1Haut.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ1Haut.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ1Haut.setTextFill(Paint.valueOf("#ffffff"));
        btnJ1Haut.setMinSize(60,35);
        btnJ1Haut.setLayoutX(m.primaryStage.getWidth()/2-50);
        btnJ1Haut.setLayoutY(m.primaryStage.getHeight()/4+50);
        btnJ1Haut.setOnMouseClicked(new KeyBinding(m, this, btnJ1Haut));
        grpSettings.getChildren().add(btnJ1Haut);

        btnJ1Bas = new Button(getSpecialChar(m.conf.getDOWNJ1()));
        btnJ1Bas.setMinSize(60,35);
        btnJ1Bas.getStyleClass().add("btnTouche");
        btnJ1Bas.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ1Bas.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ1Bas.setTextFill(Paint.valueOf("#ffffff"));
        btnJ1Bas.setLayoutX(m.primaryStage.getWidth()/2-50);
        btnJ1Bas.setLayoutY(m.primaryStage.getHeight()/4+100);
        btnJ1Bas.setOnMouseClicked(new KeyBinding(m, this, btnJ1Bas));
        grpSettings.getChildren().add(btnJ1Bas);

        btnJ1Gauche = new Button(getSpecialChar(m.conf.getLEFTJ1()));
        btnJ1Gauche.setMinSize(60,35);
        btnJ1Gauche.getStyleClass().add("btnTouche");
        btnJ1Gauche.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ1Gauche.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ1Gauche.setTextFill(Paint.valueOf("#ffffff"));
        btnJ1Gauche.setLayoutX(m.primaryStage.getWidth()/2-130);
        btnJ1Gauche.setLayoutY(m.primaryStage.getHeight()/4+100);
        btnJ1Gauche.setOnMouseClicked(new KeyBinding(m, this, btnJ1Gauche));
        grpSettings.getChildren().add(btnJ1Gauche);

        btnJ1Droite = new Button(getSpecialChar(m.conf.getRIGHTJ1()));
        btnJ1Droite.setMinSize(60,35);
        btnJ1Droite.getStyleClass().add("btnTouche");
        btnJ1Droite.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ1Droite.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ1Droite.setTextFill(Paint.valueOf("#ffffff"));
        btnJ1Droite.setLayoutX(m.primaryStage.getWidth()/2+30);
        btnJ1Droite.setLayoutY(m.primaryStage.getHeight()/4+100);
        btnJ1Droite.setOnMouseClicked(new KeyBinding(m, this, btnJ1Droite));
        grpSettings.getChildren().add(btnJ1Droite);

        j2Label = new Label("Joueur 2 :");
        j2Label.setLayoutX(m.primaryStage.getWidth()/2-120);
        j2Label.setLayoutY(m.primaryStage.getHeight()/2-25);
        j2Label.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        j2Label.setTextFill(Paint.valueOf("#ffffff"));
        j2Label.setEffect(m.glow);
        grpSettings.getChildren().add(j2Label);

        btnJ2Haut = new Button(getSpecialChar(m.conf.getUPJ2()));
        btnJ2Haut.setMinSize(60,35);
        btnJ2Haut.getStyleClass().add("btnTouche");
        btnJ2Haut.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ2Haut.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ2Haut.setTextFill(Paint.valueOf("#ffffff"));
        btnJ2Haut.setLayoutX(m.primaryStage.getWidth()/2-50);
        btnJ2Haut.setLayoutY(m.primaryStage.getHeight()/2+50);
        btnJ2Haut.setOnMouseClicked(new KeyBinding(m, this, btnJ2Haut));
        grpSettings.getChildren().add(btnJ2Haut);

        btnJ2Bas = new Button(getSpecialChar(m.conf.getDOWNJ2()));
        btnJ2Bas.setMinSize(60,35);
        btnJ2Bas.getStyleClass().add("btnTouche");
        btnJ2Bas.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ2Bas.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ2Bas.setTextFill(Paint.valueOf("#ffffff"));
        btnJ2Bas.setLayoutX(m.primaryStage.getWidth()/2-50);
        btnJ2Bas.setLayoutY(m.primaryStage.getHeight()/2+100);
        btnJ2Bas.setOnMouseClicked(new KeyBinding(m, this, btnJ2Bas));
        grpSettings.getChildren().add(btnJ2Bas);

        String test = getSpecialChar(m.conf.getLEFTJ2());
        btnJ2Gauche = new Button(test);
        btnJ2Gauche.setMinSize(60,35);
        btnJ2Gauche.getStyleClass().add("btnTouche");
        btnJ2Gauche.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ2Gauche.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ2Gauche.setTextFill(Paint.valueOf("#ffffff"));
        btnJ2Gauche.setLayoutX(m.primaryStage.getWidth()/2-130);
        btnJ2Gauche.setLayoutY(m.primaryStage.getHeight()/2+100);
        btnJ2Gauche.setOnMouseClicked(new KeyBinding(m, this, btnJ2Gauche));
        grpSettings.getChildren().add(btnJ2Gauche);

        btnJ2Droite = new Button(getSpecialChar(m.conf.getRIGHTJ2()));
        btnJ2Droite.setMinSize(60,35);
        btnJ2Droite.getStyleClass().add("btnTouche");
        btnJ2Droite.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnJ2Droite.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 20));
        btnJ2Droite.setTextFill(Paint.valueOf("#ffffff"));
        btnJ2Droite.setLayoutX(m.primaryStage.getWidth()/2+30);
        btnJ2Droite.setLayoutY(m.primaryStage.getHeight()/2+100);
        btnJ2Droite.setOnMouseClicked(new KeyBinding(m, this, btnJ2Droite));
        grpSettings.getChildren().add(btnJ2Droite);

        btnValid = new Button("Valider");
        btnValid.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.conf.setUPJ1(getSpecialChar(btnJ1Haut.getText()));
                m.conf.setUPJ2(getSpecialChar(btnJ2Haut.getText()));
                m.conf.setDOWNJ1(getSpecialChar(btnJ1Bas.getText()));
                m.conf.setDOWNJ2(getSpecialChar(btnJ2Bas.getText()));
                m.conf.setLEFTJ1(getSpecialChar(btnJ1Gauche.getText()));
                m.conf.setLEFTJ2(getSpecialChar(btnJ2Gauche.getText()));
                m.conf.setRIGHTJ1(getSpecialChar(btnJ1Droite.getText()));
                m.conf.setRIGHTJ2(getSpecialChar(btnJ2Droite.getText()));
                m.profil.setUsername(username.getText());
                m.conf.saveFileConf();
                m.profil.saveFileProfil();
                m.primaryStage.setScene(m.sceneMenu);
            }
        });
        btnValid.setId("btnValid");
        btnValid.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnValid.setBackground(null);
        btnValid.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnValid.setMinSize(150,50);
        btnValid.setLayoutX(m.primaryStage.getWidth()/2-120);
        btnValid.setLayoutY(m.primaryStage.getHeight()-150);
        grpSettings.getChildren().add(btnValid);

    }

    public String getSpecialChar(String toucheAchanger){
        String t = "";
        if (toucheAchanger.equalsIgnoreCase("UP")){
            t="^";
        }else if (toucheAchanger.equalsIgnoreCase("DOWN")){
            t="v";
        }else if (toucheAchanger.equalsIgnoreCase("LEFT")){
            t="<";
        }else if (toucheAchanger.equalsIgnoreCase("RIGHT")){
            t=">";
        }else if (toucheAchanger.equalsIgnoreCase("^")){
            t="UP";
        }else if (toucheAchanger.equalsIgnoreCase("v")){
            t="DOWN";
        }else if (toucheAchanger.equalsIgnoreCase("<")){
            t="LEFT";
        }else if (toucheAchanger.equalsIgnoreCase(">")){
            t="RIGHT";
        }else{
            t= toucheAchanger;
        }
        return t;
    }
}
