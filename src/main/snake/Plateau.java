package main.snake;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.Random;

public class Plateau extends Rectangle {

    private ArrayList<Obstacle> listObstacle;
    private Group group;

    public Plateau(Group group, int width, int height){
        super(width,height);
        this.group = group;
        listObstacle = new ArrayList<>();

    }
    public void creerPlateauPampa(){
        this.setFill(new ImagePattern(new Image("main/resources/image/monde1.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_pierre.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_pierre.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_pierre.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_pierre.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_pierre.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_pierre.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_souche.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_souche.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_souche.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_souche.png")));


    }
    public void creerPlateauAbysses(){
        this.setFill(new ImagePattern(new Image("main/resources/image/monde2.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_bateau.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_bateau.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_bateau.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_bateau.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_bateau.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_bateau.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_corail.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_corail.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_corail.png")));
        listObstacle.add(new Obstacle(group,this,new Image("main/resources/image/obstacle_corail.png")));

    }
    public void creerPlateauThrone(){
        this.setFill(new ImagePattern(new Image("main/resources/image/monde3.png")));

    }
    public void creerPlateauJoueurContreJoueur(){
        ArrayList<ImagePattern> background = new ArrayList<>();
        background.add(new ImagePattern(new Image("main/resources/image/monde1.png")));
        background.add(new ImagePattern(new Image("main/resources/image/monde2.png")));
        background.add(new ImagePattern(new Image("main/resources/image/monde3.png")));
        Random rd = new Random();
        this.setFill(background.get(rd.nextInt(2)));
    }
    public void affichePlateau(){
       this.group.getChildren().add(this);
        for (Component obstacle: listObstacle) {
            obstacle.spawn();
        }
    }
    public ArrayList<Obstacle> getListObstacle(){
        return listObstacle;
    }

}
