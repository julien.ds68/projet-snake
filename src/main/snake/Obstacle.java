package main.snake;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

public class Obstacle extends Component {
    private Image image;
    private float radius=40;
    private int rdw;
    private int rdh;

    public Obstacle(Group group, Plateau plateau,Image image) {
        super(group, plateau);
        this.image=image;
    }
    public void spawn(){
        do {
            rdw = (int) (plateau.getX() + (Math.random() * (plateau.getX()+plateau.getWidth() - plateau.getX()-20)));
            rdh = (int) (plateau.getY() + (Math.random() * (plateau.getY()+plateau.getHeight() - plateau.getY()-20)));
            rdw/=10;
            rdh/=10;
            rdw*=10;
            rdh*=10;
        }while (!verifSpawn(rdw,rdh));


        rectangle.setX(rdw);
        rectangle.setY(rdh);
        rectangle.setWidth(radius);
        rectangle.setHeight(radius);
        rectangle.setFill(new ImagePattern(image));

        group.getChildren().add(rectangle);


    }
    private boolean verifSpawn(int rdw, int rdh){
        if ((double)rdw%8!=0 || (double)rdw%5!=0 || (double)rdh%8!=0 || (double)rdh%5!=0){
            return false;
        }
        for (Component obstacle:plateau.getListObstacle()) {
            if ((rdw < obstacle.getRectangle().getX() + 40) && (rdw > obstacle.getRectangle().getX() - 30) && (rdh < obstacle.getRectangle().getY() + 40) && (rdh > obstacle.getRectangle().getY() - 30)) {
                return false;

            }
        }
        return true;


    }
}
