package main.snake;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

public class ChooseMode implements EventHandler<ActionEvent> {
    Main m;

    Label labGeneral, labMode1, labMode2;

    Button btnBack, btnMode1, btnMode2;

    Group grpChooseMode;
    Play1v1 play1v1;

    Scene sceneChooseMode;

    private double xOffset,yOffset;


    public ChooseMode(Main main) {
        this.m = main;

    }


    @Override
    public void handle(ActionEvent event) {
        m.profil.loadStats();
        m.reloadAffStat(m.profil.getLevel(), m.profil.getExperience(), m.profil.getExpToNextLevel());
        grpChooseMode = new Group();
        sceneChooseMode = new Scene(grpChooseMode, Color.BLACK);

        btnBack = new Button("Retour");
        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                m.creerMenu();
            }
        });
        btnBack.setId("btnBack");
        btnBack.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnBack.setBackground(null);
        btnBack.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnBack.setMinSize(150,50);
        btnBack.setLayoutX(100);
        btnBack.setLayoutY(50);
        grpChooseMode.getChildren().add(btnBack);



        if (m.isFullScreen){
            m.primaryStage.setMaximized(true);
            sceneChooseMode.setOnMousePressed(null);
            sceneChooseMode.setOnMouseDragged(null);
        }else {
            m.primaryStage.setMaximized(false);
            sceneChooseMode.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = m.primaryStage.getX() - event.getScreenX();
                    yOffset = m.primaryStage.getY() - event.getScreenY();
                }
            });

            sceneChooseMode.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    m.primaryStage.setX(event.getScreenX() + xOffset);
                    m.primaryStage.setY(event.getScreenY() + yOffset);
                }
            });
        }


        grpChooseMode.getChildren().add(m.btnMinimize);
        grpChooseMode.getChildren().add(m.btnCrossLeave);




        grpChooseMode.getChildren().add(m.lvl);
        grpChooseMode.getChildren().add(m.xp);
        grpChooseMode.getChildren().add(m.xpBar);

        labGeneral = new Label("Choisir un mode");

        labMode1 = new Label("Joueur vs Joueur");
        labMode2 = new Label("Joueur vs IA");



        labGeneral.setTextFill(Paint.valueOf("#ffffff"));
        labGeneral.setLayoutX(400);
        labGeneral.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 70));
        labGeneral.setLayoutY(150);

        labMode1.setTextFill(Paint.valueOf("#ffffff"));
        labMode1.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        labMode1.setLayoutX(100);
        labMode1.setLayoutY(300);

        labMode2.setTextFill(Paint.valueOf("#ffffff"));
        labMode2.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        labMode2.setLayoutX(800);
        labMode2.setLayoutY(300);


        grpChooseMode.getChildren().add(labGeneral);
        grpChooseMode.getChildren().add(labMode1);
        grpChooseMode.getChildren().add(labMode2);

        btnMode1 = new Button();
        btnMode2 = new Button();

        ImageView img = new ImageView(new Image(getClass().getResource("../resources/image/PlayerVSPlayer.png").toExternalForm()));
        img.setFitWidth(250);
        img.setFitHeight(250);
        btnMode1.setMinWidth(250);
        btnMode1.setMinHeight(250);
        btnMode1.setLayoutX(100);
        btnMode1.setLayoutY(350);
        btnMode1.getStyleClass().add("btnChooseMode");
        btnMode1.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnMode1.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnMode1.setTextFill(Paint.valueOf("#ffffff"));
        btnMode1.setOnAction(new Play1v1(m,Mode.PLAYER));
        btnMode1.setTextFill(Color.WHITE);
        btnMode1.setBackground(null);
        btnMode1.setGraphic(img);

        ImageView ia = new ImageView(new Image(getClass().getResource("../resources/image/PlayerVSRobot.png").toExternalForm()));
        ia.setFitWidth(250);
        ia.setFitHeight(250);
        btnMode2.setMinWidth(250);
        btnMode2.setMinHeight(250);
        btnMode2.setLayoutX(800);
        btnMode2.setLayoutY(350);
        btnMode2.getStyleClass().add("btnChooseMode");
        btnMode2.getStylesheets().addAll(getClass().getResource("../resources/css/Menu.css").toExternalForm());
        btnMode2.setFont(Font.loadFont(getClass().getResource("../resources/font/FontMenu.ttf").toExternalForm(), 50));
        btnMode2.setTextFill(Color.WHITE);
        btnMode2.setOnAction(new Play1v1(m,Mode.IA));
        btnMode2.setBackground(null);
        btnMode2.setGraphic(ia);


        grpChooseMode.getChildren().add(btnMode1);
        grpChooseMode.getChildren().add(btnMode2);

        m.primaryStage.setScene(sceneChooseMode);


    }
}
