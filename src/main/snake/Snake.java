package main.snake;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;


public class Snake extends Component {
    private int point = 0;

    private Main m;
    private ArrayList<Rectangle> corps = new ArrayList<Rectangle>();
    private Rectangle head = new Rectangle();
    private Image imgHead;
    private Image imgCorp;
    private Image imgAngle;
    private Image imgQueue;
    private String skinName;
    private ArrayList<Double> xVirage;
    private ArrayList<Double> yVirage;
    private ArrayList<Direction> directionsList;
    private String name;



    private int speedX = 20, speedY = 0;

    private boolean alive=true;

    private Label labNom;


    private boolean inPause=false;
    private Direction direction= Direction.RIGHT;


    public Snake(Main main,Group group, Plateau plateau, String defaultSkin){
        super(group,plateau);
        this.m = main;
        xVirage = new ArrayList<>();
        yVirage = new ArrayList<>();
        directionsList = new ArrayList<>();
        setSkin(defaultSkin);
    }

    public void direction(Direction d){
        if (direction!=d && !isInPause() && isAlive()){
            switch (d){
                case UP: if (direction!=Direction.DOWN || getLenght()==1){
                    speedX = 0;
                    speedY = -20;
                    head.setRotate(-90);
                    if (corps.size() > 1) {
                        xVirage.add(head.getX());
                        yVirage.add(head.getY());
                        directionsList.add(Direction.UP);
                    }
                    this.direction=d;
                }
                break;
                case DOWN: if (direction!=Direction.UP || getLenght()==1){
                    speedX = 0;
                    speedY = 20;
                    head.setRotate(90);
                    if (corps.size() > 1) {
                        xVirage.add(head.getX());
                        yVirage.add(head.getY());
                        directionsList.add(Direction.DOWN);
                    }
                    this.direction=d;
                }
                break;

                case RIGHT: if (direction!=Direction.LEFT || getLenght()==1){
                    speedX = 20;
                    speedY = 0;
                    head.setRotate(0);
                    if (corps.size() > 1){
                        xVirage.add(head.getX());
                        yVirage.add(head.getY());
                        directionsList.add(Direction.RIGHT);
                    }
                    this.direction=d;

                }
                break;

                case LEFT: if (direction!=Direction.RIGHT || getLenght()==1){
                    speedX = -20;
                    speedY = 0;
                    head.setRotate(180);
                    if (corps.size() > 1) {
                        xVirage.add(head.getX());
                        yVirage.add(head.getY());
                        directionsList.add(Direction.LEFT);
                    }
                    this.direction=d;
                }
                break;

            }

        }

    }

    public ArrayList<Rectangle> getCorps(){
        return this.corps;
    }
    public Rectangle getUnePartie(int i){
        return this.corps.get(i);
    }
    public void createHead(double x, double y, String name){
        this.name = name;
        labNom = new Label(name);
        labNom.setTextFill(Color.WHITE);
        labNom.setLayoutX(x-this.labNom.getWidth()/2);
        labNom.setLayoutY(y-15);
        group.getChildren().add(labNom);
        this.head = new Rectangle();
        this.head.setWidth(20);
        this.head.setHeight(20);
        this.head.setX(x);
        this.head.setY(y);
        this.head.setFill(new ImagePattern(imgHead));
        this.corps.add(0,head);
    }

    public void show(){
        for (int i=0;i<this.corps.size() ;i++ ) {
            group.getChildren().remove(this.corps.get(i));
            group.getChildren().add(this.corps.get(i));
        }
    }

    public String getName(){
        return this.name;
    }

    public String getSkin(){
        return this.skinName;
    }

    public void update(){
        double previousx=0;
        double previousy=0;
        double actuellex=0;
        double actuelley=0;

        for (int i=0;i<this.corps.size() ;i++ ) {
            if (i==0) {
                previousx=this.corps.get(i).getX();
                previousy=this.corps.get(i).getY();
                this.corps.get(i).setX(this.corps.get(i).getX()+this.speedX);
                this.corps.get(i).setY(this.head.getY()+this.speedY);
                labNom.setLayoutX(this.head.getX()-(this.labNom.getWidth()/2));
                labNom.setLayoutY(this.head.getY()-15);
            }else{
                actuellex =  this.corps.get(i).getX();
                actuelley = this.corps.get(i).getY();
                this.corps.get(i).setX(previousx);
                this.corps.get(i).setY(previousy);
                previousx=actuellex;
                previousy=actuelley;

            }
            
        }
        if (corps.size() != 1 && xVirage.size() != 0 && yVirage.size() != 0 && directionsList.size() != 0){
            checkVirage();
        }
        reloadSkin();
        createVirage();
        
    }

    private void checkVirage() {
        Rectangle queue = corps.get(corps.size()-1);
        if (queue.getX() == xVirage.get(0) && queue.getY() == yVirage.get(0)){
            if (directionsList.get(0) == Direction.UP){
                queue.setRotate(-90);
            } else if (directionsList.get(0) == Direction.DOWN){
                queue.setRotate(90);
            } else if (directionsList.get(0) == Direction.LEFT){
                queue.setRotate(180);
            } else if (directionsList.get(0) == Direction.RIGHT){
                queue.setRotate(0);
            }
            xVirage.remove(0);
            yVirage.remove(0);
            directionsList.remove(0);
        }
    }

    public void createVirage(){
        if (corps.size() >= 3){
            for (int i = 1; i<corps.size()-1; i++){
                for (int j = 0; j<xVirage.size(); j++){
                    if (corps.get(i).getX() == xVirage.get(j) && corps.get(i).getY() == yVirage.get(j)){
                        corps.get(i).setFill(new ImagePattern(imgAngle));
                        switch (directionsList.get(j)){
                            case RIGHT:
                                if (corps.get(i).getY()-20 == corps.get(i+1).getY() && corps.get(i).getX() == corps.get(i+1).getX()){
                                    corps.get(i).setRotate(-90);
                                }else {
                                    corps.get(i).setRotate(0);
                                }
                                break;
                            case LEFT:
                                if (corps.get(i).getY()-20 == corps.get(i+1).getY() && corps.get(i).getX() == corps.get(i+1).getX()){
                                    corps.get(i).setRotate(-180);
                                }else {
                                    corps.get(i).setRotate(90);
                                }
                                break;
                            case UP:
                                if (corps.get(i).getX()-20 == corps.get(i+1).getX() && corps.get(i).getY() == corps.get(i+1).getY()){
                                    corps.get(i).setRotate(180);
                                }else {
                                    corps.get(i).setRotate(-90);
                                }
                                break;
                            case DOWN:
                                if (corps.get(i).getX()-20 == corps.get(i+1).getX() && corps.get(i).getY() == corps.get(i+1).getY()){
                                    corps.get(i).setRotate(90);
                                }else {
                                    corps.get(i).setRotate(0);
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    public void setDefaultSkin(){
        this.imgHead = new Image("main/resources/image/SnakeSkin/default_tete.png");
    }

    public void reloadSkin(){
        this.head.setFill(new ImagePattern(imgHead));
        for (int i=1;i<this.corps.size(); i++ ) {
            this.corps.get(i).setFill(new ImagePattern(imgCorp));
        }
        if (corps.size() != 1){
            this.corps.get(this.corps.size()-1).setFill(new ImagePattern(imgQueue));
        }
    }

    public void setSkin(String skinName){
        this.skinName = skinName;
        for (String skin : m.profil.getSkinsList()) {
            if (skin.equalsIgnoreCase(skinName)) {
                this.imgHead = new Image("main/resources/image/SnakeSkin/" + skinName + "_tete.png");
                this.imgCorp = new Image("main/resources/image/SnakeSkin/" + skinName + "_corps.png");
                this.imgAngle = new Image("main/resources/image/SnakeSkin/" + skinName + "_angle.png");
                this.imgQueue = new Image("main/resources/image/SnakeSkin/" + skinName + "_queue.png");
            }
        }
    }

    public void forceSkin(String skinName){
        this.imgHead = new Image("main/resources/image/SnakeSkin/" + skinName + "_tete.png");
        this.imgCorp = new Image("main/resources/image/SnakeSkin/" + skinName + "_corps.png");
        this.imgAngle = new Image("main/resources/image/SnakeSkin/" + skinName + "_angle.png");
        this.imgQueue = new Image("main/resources/image/SnakeSkin/" + skinName + "_queue.png");
    }

    public int getLenght(){
        return this.corps.size();
    }

    public void addLenght(){
        double previousx=0;
        double previousy=0;
        Rectangle rt = new Rectangle();
        rt.setWidth(20);
        rt.setHeight(20);
        rt.setFill(Color.TRANSPARENT);
        Rectangle previous = new Rectangle();
        previous=corps.get(corps.size()-1);
        rt.setY(previous.getY());
        rt.setX(previous.getX());
        reloadSkin();
        this.update();
        this.corps.add(rt);

        if (corps.size() != 1){
            checkQueue();
        }
    }

    private void checkQueue() {
        Rectangle queue = corps.get(corps.size()-1);
        Rectangle avantQueue = corps.get(corps.size()-2);
        if (queue.getX()+20 == avantQueue.getX() && queue.getY() == avantQueue.getY()){
            queue.setRotate(0);
        }else if (queue.getX()-20 == avantQueue.getX() && queue.getY() == avantQueue.getY()){
            queue.setRotate(180);
        }else if (queue.getY()+20 == avantQueue.getY() && queue.getX() == avantQueue.getX()){
            queue.setRotate(90);
        }else if (queue.getY()-20 == avantQueue.getY() && queue.getX() == avantQueue.getX()){
            queue.setRotate(-90);
        }
    }

    public void addLenght(int lenght){
        for (int i = 0; i< lenght; i++){
            addLenght();
        }
    }

    public void addPoint(int point){
        this.point+=point;
    }

    public int getPoint() { return this.point; }

    public Rectangle getHead() { return this.corps.get(0); }

    public boolean toucheSonCorps(){
        for (int i=1;i<this.corps.size() ;i++ ) {
            if (this.head.getX()==this.corps.get(i).getX() && this.head.getY()==this.corps.get(i).getY()) {
                this.head.setFill(Color.RED);
                this.corps.get(i).setFill(Color.RED);
                alive=false;
                return true;
            }

            
        }
        return false;
    }
    public boolean sortiDuPlateau(){
        if (this.head.getX()<=plateau.getX()-20 || this.head.getY()<=plateau.getY()-20 || this.head.getX()>=plateau.getX()+plateau.getWidth() || this.head.getY()>=plateau.getY()+plateau.getHeight() ) {
            this.head.setFill(Color.RED);
            alive=false;
            return true;
        }
        return false;
    }

    public void dead(){
        alive=false;
        this.head.setFill(Color.RED);
    }
    public boolean isAlive(){
        return alive;
    }
    public void pause(){
        this.inPause=true;
    }
    public void endPause(){
        this.inPause=false;
    }
    public boolean isInPause(){
        return this.inPause;
    }


    public boolean hit(Snake snake2) {
        boolean toucher = false;
        for (int i = 1; i<snake2.getCorps().size(); i++){
            if (this.getHead().getX() == snake2.getCorps().get(i).getX() && this.getHead().getY() == snake2.getCorps().get(i).getY()){
                toucher = true;
            }
        }
        return toucher;
    }
}