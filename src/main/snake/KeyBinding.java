package main.snake;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;

public class KeyBinding implements EventHandler<MouseEvent> {

    private Main m;
    private Button btn;
    private Settings s;

    public KeyBinding(Main main, Settings settings, Button button){
        this.m = main;
        this.btn = button;
        this.s = settings;
    }

    @Override
    public void handle(MouseEvent e) {
        s.sceneSettings.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                btn.setText(s.getSpecialChar(event.getCode().toString()));
            }
        });
        s.sceneSettings.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                btn.setTextFill(Paint.valueOf("#FFF"));
                s.sceneSettings.setOnKeyPressed(null);
                s.sceneSettings.setOnKeyReleased(null);
            }
        });
    }
}
