package main.snake;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;

public class Config {

    private Main m;

    File file;
    PrintWriter log;

    private HashMap<String,String> j1Key;
    private HashMap<String,String> j2Key;

    public Config(Main main){
        this.m =main;
        j1Key = new HashMap<>();
        j2Key = new HashMap<>();
        creerFichierConfig("Configuration");
        loadConfig();
    }

    private void creerFichierConfig(String nom) {
        file = new File(nom + ".json");
        try {
            if (!file.exists()){
                System.out.println("Fichier non existant");
                file.createNewFile();
                setDefaultConfig();
            }else{
                System.out.println("Fichier existant");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setDefaultConfig() {
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject json = new JSONObject();
            JSONArray j1 = new JSONArray();
            j1.add("GoUp: UP");
            j1.add("GoDown: DOWN");
            j1.add("GoLeft: LEFT");
            j1.add("GoRight: RIGHT");

            JSONArray j2 = new JSONArray();
            j2.add("GoUp: Z");
            j2.add("GoDown: S");
            j2.add("GoLeft: Q");
            j2.add("GoRight: D");

            json.put("Joueur 2", j2);
            json.put("Joueur 1", j1);

            try (FileWriter fileW = new FileWriter(file.getName())) {
                fileW.write(json.toJSONString());
                System.out.println("Successfully Copied JSON Object to File...");
                System.out.println("\nJSON Object: " + json);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadConfig() {
        JSONParser jsonParser = new JSONParser();

        try {
            Object obj = jsonParser.parse(new FileReader(file.getName()));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray j1Touche = (JSONArray) jsonObject.get("Joueur 1");
            JSONArray j2Touche = (JSONArray) jsonObject.get("Joueur 2");
            for (int i = 0; i< j1Touche.size(); i++){
                String t = (String) j1Touche.get(i);
                if (t.startsWith("GoUp: ")){
                    j1Key.put("UP", t.replace("GoUp: ", ""));
                }else if (t.startsWith("GoDown: ")){
                    j1Key.put("DOWN", t.replace("GoDown: ", ""));
                }else if (t.startsWith("GoLeft: ")){
                    j1Key.put("LEFT", t.replace("GoLeft: ", ""));
                }else if (t.startsWith("GoRight: ")){
                    j1Key.put("RIGHT", t.replace("GoRight: ", ""));
                }
            }

            for (int i = 0; i< j2Touche.size(); i++){
                String t = (String) j2Touche.get(i);
                if (t.startsWith("GoUp: ")){
                    j2Key.put("UP", t.replace("GoUp: ", ""));
                }else if (t.startsWith("GoDown: ")){
                    j2Key.put("DOWN", t.replace("GoDown: ", ""));
                }else if (t.startsWith("GoLeft: ")){
                    j2Key.put("LEFT", t.replace("GoLeft: ", ""));
                }else if (t.startsWith("GoRight: ")){
                    j2Key.put("RIGHT", t.replace("GoRight: ", ""));
                }
            }

            System.out.println("J1: "+j1Key.toString());
            System.out.println("J2: "+j2Key.toString());


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getUPJ1(){
        return j1Key.get("UP");
    }
    public String getUPJ2(){
        return j2Key.get("UP");
    }
    public String getDOWNJ1(){
        return j1Key.get("DOWN");
    }
    public String getDOWNJ2(){
        return j2Key.get("DOWN");
    }
    public String getLEFTJ1(){
        return j1Key.get("LEFT");
    }
    public String getLEFTJ2(){
        return j2Key.get("LEFT");
    }
    public String getRIGHTJ1(){
        return j1Key.get("RIGHT");
    }
    public String getRIGHTJ2(){
        return j2Key.get("RIGHT");
    }

    public void setUPJ1(String key){
        j1Key.put("UP", key);
    }
    public void setUPJ2(String key){
        j2Key.put("UP", key);
    }
    public void setDOWNJ1(String key){
        j1Key.put("DOWN", key);
    }
    public void setDOWNJ2(String key){
        j2Key.put("DOWN", key);
    }
    public void setLEFTJ1(String key){
        j1Key.put("LEFT", key);
    }
    public void setLEFTJ2(String key){
        j2Key.put("LEFT", key);
    }
    public void setRIGHTJ1(String key){
        j1Key.put("RIGHT", key);
    }
    public void setRIGHTJ2(String key){
        j2Key.put("RIGHT", key);
    }

    public void saveFileConf(){
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject json = new JSONObject();
            JSONArray j1 = new JSONArray();
            j1.add("GoUp: "+getUPJ1());
            j1.add("GoDown: "+getDOWNJ1());
            j1.add("GoLeft: "+getLEFTJ1());
            j1.add("GoRight: "+getRIGHTJ1());

            JSONArray j2 = new JSONArray();
            j2.add("GoUp: "+getUPJ2());
            j2.add("GoDown: "+getDOWNJ2());
            j2.add("GoLeft: "+getLEFTJ2());
            j2.add("GoRight: "+getRIGHTJ2());

            json.put("Joueur 2", j2);
            json.put("Joueur 1", j1);

            try (FileWriter fileW = new FileWriter(file.getName())) {
                fileW.write(json.toJSONString());
                System.out.println("Successfully saved JSON Object to File...");
                System.out.println("\nJSON Object: " + json);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
