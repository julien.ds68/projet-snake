package main.snake;


import javafx.scene.Group;
import javafx.scene.shape.Rectangle;


abstract class Component {
    protected Group group;
    protected Plateau plateau;
    protected Rectangle rectangle;

    public Component(Group group, Plateau plateau){
        this.group = group;
        this.plateau =plateau;
        rectangle = new Rectangle();

    }
    public void spawn(){}
    public Rectangle getRectangle(){
        return rectangle;
    }
}
