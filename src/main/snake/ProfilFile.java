package main.snake;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class ProfilFile {

    private Main m;

    File file;
    PrintWriter log;
    HashMap<String, String> stats;
    ArrayList<String> skinsList;
    ArrayList<String> allSkinList;

    public ProfilFile(Main main){
        this.m =main;
        stats = new HashMap<>();
        allSkinList = new ArrayList<>();
        loadAllSkinList();
        creerFichierStats("Stats");

        loadStats();
        tryUnlockSkin();
    }

    public void loadAllSkinList() {
        File file = new File("src/main/resources/image/SnakeSkin");

        File[] skins = file.listFiles();
        for (int i = 0; i<skins.length; i++){
            if (!skins[i].getName().startsWith(".")){
                String[] skinname = skins[i].getName().split("_");
                if (!allSkinList.contains(skinname[0])){
                    allSkinList.add(skinname[0]);
                }
            }
        }
        System.out.println("Skin debloquable : " + allSkinList);
    }

    public ArrayList<String> getAllSkin(){
        return this.allSkinList;
    }

    public void tryUnlockSkin(){
        if (getLevel() >= 50){
            addSkin("developpeur");
        }
        if(getLevel() >= 20){
            addSkin("thrones");
        }
        if (getLevel() >= 10){
            addSkin("abysses");
        }
    }

    private void creerFichierStats(String nom) {
        file = new File(nom + ".json");
        try {
            if (!file.exists()){
                System.out.println("Fichier non existant");
                file.createNewFile();
                setDefaultStats();
            }else{
                System.out.println("Fichier existant");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setDefaultStats() {
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject json = new JSONObject();

            json.put("Username", "Robert");
            json.put("Level", "1");
            json.put("Experience", "0");
            json.put("ExperienceToNextLevel", "100");
            json.put("idSkinActive", "0");
            json.put("Highscore", "0");
            JSONArray skins = new JSONArray();
            skins.add("default");
            json.put("Skin List",skins);

            try (FileWriter fileW = new FileWriter(file.getName())) {
                fileW.write(json.toJSONString());
                System.out.println("Successfully write Stats File...");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadStats() {
        JSONParser jsonParser = new JSONParser();

        try {
            Object obj = jsonParser.parse(new FileReader(file.getName()));
            JSONObject jsonObject = (JSONObject) obj;

            stats.put("Username", (String) jsonObject.get("Username"));
            stats.put("Level", (String) jsonObject.get("Level"));
            stats.put("Experience", (String) jsonObject.get("Experience"));
            stats.put("ExperienceToNextLevel", (String) jsonObject.get("ExperienceToNextLevel"));
            stats.put("idSkinActive", (String) jsonObject.get("idSkinActive"));
            stats.put("Highscore", (String) jsonObject.get("Highscore"));

            skinsList = new ArrayList<>();
            JSONArray listOfSkinOwn = (JSONArray) jsonObject.get("Skin List");
            for (int i = 0; i<listOfSkinOwn.size(); i++){
                skinsList.add((String) listOfSkinOwn.get(i));
            }
            System.out.println("Stats charger : " + stats.toString() + "\nSkins Charger : " + skinsList.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void saveFileProfil(){
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject json = new JSONObject();

            json.put("Username", stats.get("Username"));
            json.put("Level", stats.get("Level"));
            json.put("Experience", stats.get("Experience"));
            json.put("ExperienceToNextLevel", stats.get("ExperienceToNextLevel"));
            json.put("idSkinActive", stats.get("idSkinActive"));
            json.put("Highscore", stats.get("Highscore"));

            JSONArray skins = new JSONArray();
            for (String skin : skinsList){
                skins.add(skin);
            }
            json.put("Skin List",skins);

            try (FileWriter fileW = new FileWriter(file.getName())) {
                fileW.write(json.toJSONString());
                System.out.println("Successfully saved JSON Object to File...");
                System.out.println("\nJSON Object: " + json);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return stats.get("Username");
    }

    public void setUsername(String newUsername) {
        stats.put("Username", newUsername);
    }

    public int getLevel(){ return Integer.parseInt(stats.get("Level")); }

    public void setLevel(int level){ stats.put("Level", String.valueOf(level)); }

    public int getExperience(){ return Integer.parseInt(stats.get("Experience")); }

    public void incrementerExperience(int nbAjouter){
        int newxp = getExperience()+nbAjouter;
        stats.put("Experience", String.valueOf(newxp));
        checkLevel();
    }

    public void incrementerLevel(int nbAjouter) {
        stats.put("Level", String.valueOf(getLevel()+nbAjouter));
    }

    public void setExperience(int newExperience){ stats.put("Experience", String.valueOf(newExperience)); }

    public int getExpToNextLevel(){ return Integer.parseInt(stats.get("ExperienceToNextLevel")); }

    public void setExpToNextLevel(int newExpToNextLevel){ stats.put("ExperienceToNextLevel", String.valueOf(newExpToNextLevel)); }

    public ArrayList<String> getSkinsList(){ return this.skinsList; }

    public void addSkin(String skinName){
        if (!skinsList.contains(skinName)){
            System.out.println("Ajout de : " + skinName);
            skinsList.add(skinName);
            saveFileProfil();
        }else{
            System.out.println("Vous possedez deja le skin : " + skinName);
        }
    }

    public int getSkinActivated(){
        return Integer.parseInt(stats.get("idSkinActive"));
    }

    public void setSkinActivate(int identifiant){
        if (getSkinsList().size() < identifiant){
            return;
        }else{
            stats.put("idSkinActive", String.valueOf(identifiant));
        }
    }

    public void checkLevel(){
        if (getExpToNextLevel() > getExperience()){
            stats.put("Experience", String.valueOf(getExperience()));
        }else{
            if (getExperience() == getExpToNextLevel()){
                incrementerLevel(1);
                setExperience(0);
                setExpToNextLevel((int) Math.round((getExpToNextLevel()*1.25)-(getExpToNextLevel()*1.25)%10));
            }else{
                int xprestant = getExperience()-getExpToNextLevel();
                incrementerLevel(1);
                setExpToNextLevel((int) Math.round((getExpToNextLevel()*1.25)-(getExpToNextLevel()*1.25)%10));
                while (xprestant>=getExpToNextLevel()){
                    xprestant=xprestant-getExpToNextLevel();
                    incrementerLevel(1);
                    setExpToNextLevel((int) Math.round((getExpToNextLevel()*1.25)-(getExpToNextLevel()*1.25)%10));
                }
                setExperience(xprestant);
            }
        }
    }

    public int getHighscore(){
        return Integer.parseInt(stats.get("Highscore"));
    }

    public void tryChangeHighscore(int highscore){
        if (getHighscore() < highscore){
            stats.put("Highscore", String.valueOf(highscore));
        }
    }
}
