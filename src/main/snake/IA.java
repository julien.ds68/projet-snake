package main.snake;

import javafx.scene.shape.Rectangle;

import java.util.Random;

public class IA {

    private Snake snake;
    private Snake j2;
    private Plateau plateau;
    private Apple apple;
    private Direction directionActu;

    public IA(Snake snake, Snake opposant,Plateau plateau, Apple apple, Direction defaultDirection) {
        this.snake = snake;
        this.j2 = opposant;
        this.plateau = plateau;
        this.apple = apple;
        this.directionActu = defaultDirection;
    }

    public void play(boolean grow) {

//        allerPomme();
//        eviterJoueur2();
//        checkMur();
        survive();
        snake.direction(directionActu);

        if (grow){
            snake.addLenght();
        }else {
            snake.update();
        }
        snake.show();
    }

    private void eviterJoueur2() {
        for (Rectangle r : j2.getCorps()) {
            if (snake.getHead().getX() + 10 == r.getX() && snake.getHead().getY() == r.getY()) {
                directionActu = Direction.LEFT;
//                snake.direction(Direction.LEFT);
            } else if (snake.getHead().getX() - 10 == r.getX() && snake.getHead().getY() == r.getY()) {
                directionActu = Direction.RIGHT;
//                snake.direction(Direction.RIGHT);
            } else if (snake.getHead().getY() + 10 == r.getY() && snake.getHead().getX() == r.getX()) {
                directionActu = Direction.UP;
//                snake.direction(Direction.UP);
            } else if (snake.getHead().getY() - 10 == r.getY() && snake.getHead().getX() == r.getX()) {
                directionActu = Direction.DOWN;
//                snake.direction(Direction.DOWN);
            }
        }
    }

    public void setApple(Apple apple) {
        this.apple = apple;
    }

    public void checkMur() {
        if (snake.getLenght() == 1){
            if (snake.getHead().getX() + 10 >= plateau.getX() + plateau.getWidth()) {
                directionActu = Direction.LEFT;
//            snake.direction(Direction.LEFT);
            } else if (snake.getHead().getX() - 10 <= plateau.getX()) {
                directionActu = Direction.RIGHT;
//            snake.direction(Direction.RIGHT);
            } else if (snake.getHead().getY() + 10 >= plateau.getY() + plateau.getHeight()) {
                directionActu = Direction.UP;
//            snake.direction(Direction.UP);
            } else if (snake.getHead().getY() - 10 <= plateau.getY()) {
                directionActu = Direction.DOWN;
//            snake.direction(Direction.DOWN);
            }
        }else{
            if (snake.getHead().getX() + 10 >= plateau.getX() + plateau.getWidth()) {
                directionActu = Direction.DOWN;
//            snake.direction(Direction.LEFT);
            } else if (snake.getHead().getX() - 10 <= plateau.getX()) {
                directionActu = Direction.DOWN;
//            snake.direction(Direction.RIGHT);
            } else if (snake.getHead().getY() + 10 >= plateau.getY() + plateau.getHeight()) {
                directionActu = Direction.LEFT;
//            snake.direction(Direction.UP);
            } else if (snake.getHead().getY() - 10 <= plateau.getY()) {
                directionActu = Direction.LEFT;
//            snake.direction(Direction.DOWN);
            }
        }
    }

    public void survive(){
        switch (directionActu){
            case UP:
                if (snake.getHead().getY()-20 <= plateau.getY()-20){
                    if (snake.getHead().getX()+20 >= plateau.getX()+plateau.getWidth()){
                        directionActu = Direction.LEFT;
                    }else if (snake.getHead().getX()-20 <= plateau.getX()){
                        directionActu = Direction.RIGHT;
                    }else{
                        directionActu = Direction.RIGHT;
                    }
                }else{
                    boolean changed = false;
                    for (Rectangle rt : j2.getCorps()){
                        if (rt.getY() == snake.getHead().getY()-20 && rt.getX() == snake.getHead().getX()){
                            for (Rectangle rt2 : j2.getCorps()){
                                if (rt2.getX() == snake.getHead().getX()+20 && rt2.getY() == snake.getHead().getY()){
                                    changed = true;
                                    directionActu = Direction.LEFT;
                                }else if(rt2.getX() == snake.getHead().getX()-20 && rt2.getY() == snake.getHead().getY()){
                                    changed = true;
                                    directionActu = Direction.RIGHT;
                                }else{
                                    changed = true;
                                    directionActu = Direction.RIGHT;
                                }
                            }
                        }
                    }
                    if (!changed){
                        directionActu = Direction.UP;
                    }
                }
                break;
            case DOWN:
                if (snake.getHead().getY()+20 >= plateau.getY()+plateau.getHeight()){
                    if (snake.getHead().getX()+20 >= plateau.getX()+plateau.getWidth()){
                        directionActu = Direction.LEFT;
                    }else if (snake.getHead().getX()-20 <= plateau.getX()){
                        directionActu = Direction.RIGHT;
                    }else{
                        directionActu = Direction.RIGHT;
                    }
                }else{
                    boolean changed = false;
                    for (Rectangle rt : j2.getCorps()){
                        if (rt.getY() == snake.getHead().getY()+20 && rt.getX() == snake.getHead().getX()){
                            for (Rectangle rt2 : j2.getCorps()){
                                if (rt2.getX() == snake.getHead().getX()+20 && rt2.getY() == snake.getHead().getY()){
                                    changed = true;
                                    directionActu = Direction.LEFT;
                                }else if(rt2.getX() == snake.getHead().getX()-20 && rt2.getY() == snake.getHead().getY()){
                                    changed = true;
                                    directionActu = Direction.RIGHT;
                                }else{
                                    changed = true;
                                    directionActu = Direction.RIGHT;
                                }
                            }
                        }
                    }
                    if (!changed){
                        directionActu = Direction.DOWN;
                    }
                }
                break;
            case LEFT:
                if (snake.getHead().getX()-20 <= plateau.getX()-20){
                    if (snake.getHead().getY()+20 >= plateau.getY()+plateau.getHeight()){
                        directionActu = Direction.UP;
                    }else if (snake.getHead().getY()-20 <= plateau.getY()){
                        directionActu = Direction.DOWN;
                    }else{
                        directionActu = Direction.UP;
                    }
                }else{
                    boolean changed = false;
                    for (Rectangle rt : j2.getCorps()){
                        if (rt.getX() == snake.getHead().getX()-20 && rt.getY() == snake.getHead().getY()){
                            for (Rectangle rt2 : j2.getCorps()){
                                if (rt2.getY() == snake.getHead().getY()+20 && rt2.getX() == snake.getHead().getX()){
                                    changed = true;
                                    directionActu = Direction.UP;
                                }else if(rt2.getY() == snake.getHead().getY()-20 && rt2.getX() == snake.getHead().getX()){
                                    changed = true;
                                    directionActu = Direction.DOWN;
                                }else{
                                    changed = true;
                                    directionActu = Direction.UP;
                                }
                            }
                        }
                    }
                    if (!changed){
                        directionActu = Direction.LEFT;
                    }
                }
                break;
            case RIGHT:
                if (snake.getHead().getX()+20 >= plateau.getX()){
                    if (snake.getHead().getY()+20 >= plateau.getY()+plateau.getHeight()){
                        directionActu = Direction.UP;
                    }else if (snake.getHead().getY()-20 <= plateau.getY()){
                        directionActu = Direction.DOWN;
                    }else{
                        directionActu = Direction.UP;
                    }
                }else{
                    boolean changed = false;
                    for (Rectangle rt : j2.getCorps()){
                        if (rt.getX() == snake.getHead().getX()+20 && rt.getY() == snake.getHead().getY()){
                            for (Rectangle rt2 : j2.getCorps()){
                                if (rt2.getY() == snake.getHead().getY()+20 && rt2.getX() == snake.getHead().getX()){
                                    changed = true;
                                    directionActu = Direction.UP;
                                }else if(rt2.getY() == snake.getHead().getY()-20 && rt2.getX() == snake.getHead().getX()){
                                    changed = true;
                                    directionActu = Direction.DOWN;
                                }else{
                                    changed = true;
                                    directionActu = Direction.UP;
                                }
                            }
                        }
                    }
                    if (!changed){
                        directionActu = Direction.RIGHT;
                    }
                }
                break;
        }
    }

    public void allerPomme() {
        double pommeX = apple.getApple().getX();
        double pommeY = apple.getApple().getY();
        Double[] distance = new Double[4];

        // Distance Bloc Droite Au Snake
        distance[0] = Math.abs(pommeX - snake.getHead().getX() + 10);

        // Distance Bloc En Dessous Du Snake
        distance[1] = Math.abs(pommeY - snake.getHead().getY() + 10);

        // Distance Bloc Gauche Au Snake
        distance[2] = Math.abs(pommeX - snake.getHead().getX() - 10);

        // Distance Bloc Au Dessus Du Snake
        distance[3] = Math.abs(pommeY - snake.getHead().getY() - 10);

        System.out.println("Pomme : x : " + pommeX + " y : " + pommeY);
        for (double d : distance){
            System.out.println("Distance : " + d);
        }

        if (distance[0] <= distance[1]){
            if (distance[2] >= distance[1]){
                System.out.println("Haut");
                snake.direction(Direction.UP);
            }else{
                System.out.println("Bas");
                snake.direction(Direction.DOWN);
            }
        }else{
            if (distance[3] >= distance[0]){
                System.out.println("Gauche");
                snake.direction(Direction.LEFT);
            }else{
                System.out.println("Droite");
                snake.direction(Direction.RIGHT);
            }
        }
        /*
        if (blocDroiteAuSnake <= blocEnDessousDuSnake) {
            if (Math.abs(blocGaucheAuSnake) >= blocEnDessousDuSnake) {
                System.out.println("Haut");
                if (snake.getDirection()==Direction.DOWN){
                    snake.direction(Direction.LEFT);
                }
                snake.direction(Direction.UP);
            } else {
                System.out.println("Bas");
                if (snake.getDirection()==Direction.UP){
                    snake.direction(Direction.LEFT);
                }
                snake.direction(Direction.DOWN);
            }
        } else if (blocEnDessousDuSnake < blocDroiteAuSnake) {
            if (Math.abs(blocAuDessusDuSnake) >= blocDroiteAuSnake) {
                System.out.println("Gauche");
                if (snake.getDirection()==Direction.RIGHT){
                    snake.direction(Direction.UP);
                }
                snake.direction(Direction.LEFT);
            } else {
                System.out.println("Droite");
                if (snake.getDirection()==Direction.LEFT){
                    snake.direction(Direction.UP);
                }
                snake.direction(Direction.RIGHT);
            }
        }
         */
//            if (distanceX10 <= distanceY10){
//                if (Math.abs(distanceXm10) >= distanceY10){
//                    System.out.println("Haut");
//                    if (snake.getDirection() == Direction.DOWN){
//                        snake.direction(Direction.LEFT);
//                    }else if (snake.getDirection() == Direction.LEFT){
//                        snake.direction(Direction.UP);
//                    }else if(snake.getDirection() == Direction.RIGHT){
//                        snake.direction(Direction.UP);
//                    }else{
//                        snake.direction(Direction.UP);
//                    }
//                }else{
//                    System.out.println("Bas");
//                    if (snake.getDirection() == Direction.UP){
//                        snake.direction(Direction.LEFT);
//                    }else if (snake.getDirection() == Direction.LEFT){
//                        snake.direction(Direction.DOWN);
//                    }else if(snake.getDirection() == Direction.RIGHT){
//                        snake.direction(Direction.DOWN);
//                    }else{
//                        snake.direction(Direction.DOWN);
//                    }                }
//            }else if (distanceY10 < distanceX10){
//                if (Math.abs(distanceYm10) >= distanceX10){
//                    System.out.println("Gauche");
//                    if (snake.getDirection() == Direction.DOWN){
//                        snake.direction(Direction.LEFT);
//                    }else if (snake.getDirection() == Direction.UP){
//                        snake.direction(Direction.LEFT);
//                    }else if(snake.getDirection() == Direction.RIGHT){
//                        snake.direction(Direction.DOWN);
//                    }else{
//                        snake.direction(Direction.LEFT);
//                    }
//                }else{
//                    System.out.println("Droite");
//                    if (snake.getDirection() == Direction.DOWN){
//                        snake.direction(Direction.RIGHT);
//                    }else if (snake.getDirection() == Direction.LEFT){
//                        snake.direction(Direction.UP);
//                    }else if(snake.getDirection() == Direction.UP){
//                        snake.direction(Direction.RIGHT);
//                    }else{
//                        snake.direction(Direction.RIGHT);
//                    }
//                }
//            }
    //System.exit(0);
    }

    public void choixDirection(){
        Random rd = new Random();
        switch (rd.nextInt(3)){
            case 0:
                snake.direction(Direction.UP);
                break;
            case 1:
                snake.direction(Direction.DOWN);
                break;
            case 2:
                snake.direction(Direction.RIGHT);
                break;
            case 3:
                snake.direction(Direction.LEFT);
                break;
        }
    }

}
