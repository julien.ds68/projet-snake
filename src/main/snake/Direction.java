package main.snake;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
}
