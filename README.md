## Installation & Execution

 1. Télécharger la source du code
 2. Ajouter la dépendance à json-simple v1.1.1 (si pas déjà fait pour compiler et ajoutez les dépendance en même temps : `javac -cp ".: ./json-simple-1.1.1.jar" *.java`)
 3. compiler les .java dans le dossier ```src/main/snake/``` (commande pour compiler tout les .java d'un coup : `javac *.java`)
 4. ensuite éxécuter le fichier main.class avec la commande : `java main`